var config = {};

//Статика которая отвечает за стандартные настройки
config.port = 80;
config.name = "Женя бест";
config.home = ""
config.ip="127.0.0.1"


//Параметры сессии 
config.session = {
    "secret":"SashaBest",
    "key":"sid",
    "cookie":{
        "httpOnly":true,
        "maxAge":1000*60*60    
    }
};


config.mongoose = {
    uri: "mongodb://127.0.0.1/superblog",
    options: {
        server: {
            socketOptions: {
                keepAlive: 1
            }
        }
    }
};


module.exports=config;