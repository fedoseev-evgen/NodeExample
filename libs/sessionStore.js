var session = require('express-session');
var mongoose = require('../libs/mongoose');//Связующее звено между MongoBD и Sever
var mongoStore = require('connect-mongo')(session);

var sessionStore = new mongoStore({mongooseConnection: mongoose.connection});//Хранилище сессий(его создание)

module.exports = sessionStore;