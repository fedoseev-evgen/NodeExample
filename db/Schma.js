const mongoose = require('mongoose');
const crypto = require('crypto');//Модуль для шифрования пароля

const userSchema = mongoose.Schema({//Создание самой схемы ячейки
    _id: mongoose.Schema.Types.ObjectId,
    email:{
        type:String,//Выбор типа данных в ячейке
        unique:true,//Установка флага, что это поле должно быть уникальным
        required:true//Установка флага, что это поле является обязательным
    },
    state:{
        type:String,
        default:"Ты еще не подтвердил свою почту"//Выставление значения для ячейки, если данных для нее не поступит
    },
    name: {
        type: String,
        required: true
    },
    hashPassword: {//Поле зашифрованного пароля
        type: String,
        required: true
    },
    salt: {//Соль шифрования
        type: String,
        required: true
    },
    mass:{
        type: [
            {
                _id: mongoose.Schema.Types.ObjectId,
                text:String
            }
        ],
        default: []
    }
},{
    collection: "User"//Назначени места хранения (Коллекции)
});


userSchema.virtual('password')//Создание фантомного поля пароль (Здесь вы видите пароль в последний раз)
    .set(function (password) {//Запуск функции, которая придумывает свой ключ шифрования и шифрует пароль
        this.salt = Math.random() + 'dhfjgbjkcdngvkjnbjfdgbvkigrisjsjgibgfidri\vgkli\adgfsgsgs';//Создание ключа шифрования
        this.hashPassword = this.encryptPassword(password);//Шифрования пароля
    });


userSchema.methods = {//Описание методов у ячейки
    encryptPassword: function (password) {//Вызов функции шифрования пароля по соле(ключу шифрования)
        return crypto.createHmac('sha256', this.salt).update(password).digest('hex');
    },
    checkPassword: function (password) {
        return this.encryptPassword(password) === this.hashPassword;//Функция проверяет равенли пароль, которые мы зашифровали от пользователя паролю, который хранится в БД
    },
    // plusMass: function(el){
    //     this.mass=(el);
    // }
};

userSchema.statics = {//Функции для создания ячейки, в которых идет проверка на выполнение каких-либо условий.
    checkEmail: async function (email) {

        const _email = await this.findOne({email: email}).exec();
        if (_email === null){
            return true;
        } else {
            throw Error('Почта уже зарегистрирована!');
        }
    },
    checkPassword: async function (password) {
        if (password.length < 8){
            throw Error('Пароль должен быть больше 8 символов!');
        } else {
            return true;
        }
    },
    checkName: async function (name) {
        if (name.length < 3){
            throw Error('Длинна имени должна быть больше 2 символов!');
        } else {
            return true;
        }
    },
};

userSchema.pre('save', function(next) {// Функция сохранения данных
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();
    
    next();
});

module.exports = mongoose.model('User', userSchema);