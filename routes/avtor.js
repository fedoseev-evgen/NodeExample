const fs = require('fs');
const mongoose = require('mongoose');
const userSchema = require ('./../db/Schma');
var log = require('./../libs/log')(module);


module.exports=function(req, res){
    try{
    userSchema.findOne({email:req.body.email},function(error, user) {//Поиск записи в которой поле Email будет равно полю пришедшему с фронта 
        if(error)res.status(200).send("Что-то не так");
        if(user.checkPassword(req.body.pass)){//Вызов функции из шаблона для проверки правильности пароля
        req.session.user = (user._id); //Создание сессии пользователя
        req.session.state= user.state;
        // console.log('-----------------------------------------'+req.session.user+'-------------------------------------------------------');
        res.status(200).send("Ты авторизирован");//Вывод сообщения для того чтобы не лег серв
        }else{
        res.status(200).send("Что-то не так"); 
        }
      });
    }
    catch(err){
        log.error(err);
        res.send('Авторизация прошла неудачно');
    }
}