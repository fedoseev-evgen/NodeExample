const fs = require('fs');
const mongoose = require('mongoose');
const userSchema = require ('./../db/Schma');
var log = require('./../libs/log')(module);

module.exports=function(req, res){//Создание данных для главной страницы
    try{
    if(req.session.user!==undefined){//проверка на наличие сессии
    userSchema.findById(req.session.user,function(error, user) {
        if(error)var mass=[{text:"Упс"}];else
        if(user.mass.length===0){
            var mass =[{text:"Вы еще нтчего не писали"}];
        }else
        var mass = user.mass;
        res.render('main',{state:req.session.state,session:req.session.user,mass:mass});//Рендер странички для авторизированного пользователя
    });
}else
res.render('main',{state:req.session.state,session:req.session.user,mass:[{text:"Ты еще не вошел"}]});//Рендер странички для не авторизированного пользователя
    }
    catch(err){
        log.error(err);
        res.send('что-то не так :)');
    }
}