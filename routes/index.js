const main = require('./main');
const out = require('./out');
const reg = require('./reg');
const avtor = require('./avtor');
const plus = require('./plus');
const minus =require('./minus');
const fotoSave = require('./fotoSave');
const fotoShow = require('./fotoShow');
const EmailBot = require('./EmailBot');
const regFull = require('./regFull');


//Является главным файлом в котором собранны все роуты по отдельностиы

module.exports = async function(app){
    app.get('/', main);
    app.get('/out', out);
    app.post('/reg', reg);
    app.get('/regFull/:id', regFull);
    app.post('/avtor', avtor);
    app.post('/plus', plus);
    app.post('/minus', minus);
    app.post('/fotoSave', fotoSave);
    app.get('/fotoShow',fotoShow);
    app.post('/EmailBot',EmailBot);
};