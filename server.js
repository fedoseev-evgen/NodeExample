const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
var http = require('http');
var path = require('path');
var cluster = require('cluster');//Для запуска сервера в кластерном режиме(на каждое ядро)
var compression = require('compression');//Модуль отвечающий за компрессию сервера
var config = require('./libs/config');
var session = require('express-session');
var multer = require('multer');//??
var log = require('./libs/log')(module);


const app = express();


if (cluster.isMaster) {

    var cpuCount = require('os').cpus().length;

    for (var i = 0; i < cpuCount; i += 1) {
        cluster.schedulingPolicy = cluster.SCHED_NONE;
        cluster.fork();
    }

    cluster.on('exit', function (worker) {
        console.log('Worker ' + worker.id + ' died :(');
        cluster.fork();
    });

} else {

    app.use(compression());

    app.set('view engine', 'ejs');
    app.use(bodyParser.urlencoded({
        extended: true,
        limit: '50mb'
    }));
    app.use(cookieParser());

    app.use(multer(
        {
            dest: path.join(__dirname, 'public/uploads'),//папка для хранения фронта (картинки изображения и тд.)
            limits: {
                fieldNameSize: 999999999,
                fieldSize: 999999999
            }
        }
    ).any());

    app.use(express.static(path.join(__dirname, 'public')));//Задаем папку статики для фронта
    app.use('/files', express.static('../files'));//Обработка адреса http://localhost/files/название файла из папки files

    var sessionStore = require('./libs/sessionStore');//настройка хранилища сессий
    app.use(session({///настройка сессии
        secret: config.session.secret,
        key: config.session.key,
        cookie: config.session.cookie,
        store: sessionStore
    }));


    //************************* Middleware ***********************************(временно не используется)
    // app.use(common.commonMiddleware);

    //************************* Routes ***********************************
    require('./routes')(app);//настройка роутов сайта

    //************************* GARBAGE магия ***********************************(временно не используется)
    // var gcInterval;
    // function init()
    // {
    //     gcInterval = setInterval(function() { gcDo(); }, 60000);
    // }
    // function gcDo()
    // {
    //     global.gc();
    //     clearInterval(gcInterval);
    //     init();
    // }
    // init();
    //************************************************************

    //************************* 404 ***********************************
   app.use(function(req, res){
       //Вывод сообщения об ошибке в случае, если стр не найдена
       res.locals.metatitle = '404 Ничего не найдено';
       res.locals.pagenoindex = 'yes';
       res.send('404 ошибка');
   });

    //************************* Запуск сервера ***********************************
    var httpServer = http.createServer(app);//Запуск HTTP сервера, не HTTPS!!!
    function onListening(){
        log.info('Listening on port ', config.port);
    }
    httpServer.on('listening', onListening);
    httpServer.listen(config.port, config.ip);
}